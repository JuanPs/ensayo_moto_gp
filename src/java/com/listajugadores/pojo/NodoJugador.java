/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listajugadores.pojo;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class NodoJugador implements Serializable{
    private Jugador dato;
    private NodoJugador siguiente;
    private NodoJugador anterior;

    public NodoJugador(Jugador dato) {
        this.dato = dato;
    }

    public Jugador getDato() {
        return dato;
    }

    public void setDato(Jugador dato) {
        this.dato = dato;
    }

    public NodoJugador getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoJugador siguiente) {
        this.siguiente = siguiente;
    }

    public NodoJugador getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoJugador anterior) {
        this.anterior = anterior;
    }
    
    
}
