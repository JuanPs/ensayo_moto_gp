/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listajugadores.pojo;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Pirinola implements Serializable{
    private String descripcion;
    private boolean estado;
    private boolean estado2;
    private short valor;

    public Pirinola(String descripcion, boolean estado, boolean estado2, short valor) {
        this.descripcion = descripcion;
        this.estado = estado;
        this.estado2 = estado2;
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public boolean isEstado2() {
        return estado2;
    }

    public void setEstado2(boolean estado2) {
        this.estado2 = estado2;
    }

    public short getValor() {
        return valor;
    }

    public void setValor(short valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Pirinola{" + "descripcion=" + descripcion + ", estado=" + estado + ", estado2=" + estado2 + ", valor=" + valor + '}';
    }
    
    
}
