/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listajugadores.pojo;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class NodoPirinola implements Serializable{
    private Pirinola dato;
    private NodoPirinola siguiente;
    private NodoPirinola anterior;

    public NodoPirinola(Pirinola dato) {
        this.dato = dato;
    }

    public Pirinola getDato() {
        return dato;
    }

    public void setDato(Pirinola dato) {
        this.dato = dato;
    }

    public NodoPirinola getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoPirinola siguiente) {
        this.siguiente = siguiente;
    }

    public NodoPirinola getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoPirinola anterior) {
        this.anterior = anterior;
    }
    
}
