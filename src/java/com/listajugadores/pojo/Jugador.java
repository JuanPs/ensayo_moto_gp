/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listajugadores.pojo;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Jugador implements Serializable{
    private String nombre;
    private short numeroFichas;

    public Jugador(String nombre, short numeroFichas) {
        this.nombre = nombre;
        this.numeroFichas = numeroFichas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getNumeroFichas() {
        return numeroFichas;
    }

    public void setNumeroFichas(short numeroFichas) {
        this.numeroFichas = numeroFichas;
    }

    @Override
    public String toString() {
        return "Jugador{" + "nombre=" + nombre + ", numeroFichas=" + numeroFichas + '}';
    }
    
    
}
