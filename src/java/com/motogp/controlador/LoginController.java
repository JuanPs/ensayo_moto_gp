    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.controlador;

import com.motogp.entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author juand
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {
private String correo;
    private String password;
    private Usuario usuarioLogueado;
    @EJB
    private UsuarioFacade conUsuario;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public UsuarioFacade getConUsuario() {
        return conUsuario;
    }

    public void setConUsuario(UsuarioFacade conUsuario) {
        this.conUsuario = conUsuario;
    }
    
    

 

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Usuario getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(Usuario usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }
    
    
    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {
    }
    
    public String autenticarUsuario()
    {
        usuarioLogueado = conUsuario.encontrarUsuarioxCorreo(correo);
        if(usuarioLogueado!=null)
        {
            if(usuarioLogueado.getContraseña().equals(password))
            {                
                return "ingresar";
            }
            else
            {
                FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            "Password erróneo", "Password Erróneo"));
            }    
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                            "El usuario no existe", "El usuario no existe"));
        }    
        return null;
            
    }     
    
    public String cerrarSesion()
    {
        usuarioLogueado=null;
        correo="";
        password="";
        return "cerrar";
    }
}
