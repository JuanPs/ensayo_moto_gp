/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.controlador;

import com.motogp.modelo.Nodo;
import java.io.Serializable;

/**
 *
 * @author juand
 */
public class ListaPilotosDE implements  Serializable{
    private Nodo cabeza;
    
    
    public String adicionarNodoFinal(Nodo dato) {
        if (cabeza == null) {
            cabeza = dato;
        } else {
            Nodo temp = cabeza;
            while (temp.getSiguiente() != null) {
                temp = temp.getSiguiente();
            }
            Nodo nuevo = dato;
            temp.setSiguiente(nuevo);
            nuevo.setAnterior(temp);
        }
        return "Adicionado con éxito";

    }

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    
    public void eliminarxDato(Nodo datoElim){
        if(cabeza!=null){
            Nodo temp=cabeza;
            while(temp!=null){
                if (temp.getPiloto().equals(datoElim.getPiloto()))
                {
                    if (temp.getAnterior()!=null){
                        temp.getAnterior().setSiguiente(temp.getSiguiente());
                    }
                    else{
                        cabeza=temp.getSiguiente();
                    }
                    if(temp.getSiguiente()!=null){
                        temp.getSiguiente().setAnterior(temp.getAnterior());
                    }
                    break;
                }
                temp=temp.getSiguiente();
            }
        }        
    }
    
    public Nodo obtenerMenor(){
        if (cabeza!=null){
            Nodo menor=new Nodo (cabeza.getPiloto(),cabeza.getTiempo());
            Nodo temp=cabeza;
            
            while (temp!=null){
                if (temp.getTiempo().getTime()<menor.getTiempo().getTime()){
                    menor=new Nodo (temp.getPiloto(),temp.getTiempo());
                }
                temp=temp.getSiguiente();
            }
            return menor;
        }
        return null;
    }
    
   
    
    
    
    public void ordenarTiempo(){
        if(cabeza!=null){
            ListaPilotosDE lista2=new ListaPilotosDE();
            do {
                Nodo menor=obtenerMenor();
                eliminarxDato(menor);
                lista2.adicionarNodoFinal(menor);
            
            }
            while (cabeza!=null);
            cabeza=lista2.getCabeza();
        }
    }
    
     public Nodo obtenerNodoxPosicion(int pos){
        if (cabeza != null){
            Nodo temp =cabeza;
            int cont=1;
            while (temp !=null){
                if(cont==pos)
                {
                    return new Nodo(temp.getPiloto(),temp.getTiempo());
                }
                cont ++;
                temp.getSiguiente();
                
            }
            return temp;
        }
        return null;
    }
     
     public int obtenerPosicionxNodo(Nodo nodo){
        if (cabeza != null){
            Nodo temp =cabeza;
            int cont=1;
            while (temp !=null){
                if(temp.equals(nodo))
                {
                     return cont;
                }
                cont ++;
                temp.getSiguiente();
                
            }
          }
        return -1;
    }
     public int size(){
         int cont=0;
         while(this.cabeza!=null){
             cont++;
             this.cabeza.getSiguiente();
         }
         return cont;
     }
     public void CopiarACarrera (){
          if(cabeza!=null){
              ListaPilotosDE listacopia=new ListaPilotosDE();
              do
              {
                   listacopia.adicionarNodoFinal(cabeza);
              }
                 while (cabeza!=null);
            cabeza=listacopia.getCabeza();
          }
            
       
     }
}
