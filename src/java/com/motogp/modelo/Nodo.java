/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.modelo;


import com.motogp.entidades.Piloto;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author juand
 */
public class Nodo implements Serializable{
    private Piloto piloto;
    private Date tiempo;
    
    private Nodo siguiente;
    private Nodo anterior ; 

    public Nodo(Piloto piloto, Date tiempo) {
        this.piloto = piloto;
        this.tiempo = tiempo;
        
        
    }

    public Piloto getPiloto() {
        return piloto;
    }

    public void setPiloto(Piloto piloto) {
        this.piloto = piloto;
    }

    public Date getTiempo() {
        return tiempo;
    }

    public void setTiempo(Date tiempo) {
        this.tiempo = tiempo;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }

    public Nodo getAnterior() {
        return anterior;
    }

    public void setAnterior(Nodo anterior) {
        this.anterior = anterior;
    }
    
    
}
