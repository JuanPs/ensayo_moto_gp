/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author juand
 */
@Entity
@Table(name = "piloto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Piloto.findAll", query = "SELECT p FROM Piloto p")
    , @NamedQuery(name = "Piloto.findByNumero", query = "SELECT p FROM Piloto p WHERE p.numero = :numero")
    , @NamedQuery(name = "Piloto.findByNombre", query = "SELECT p FROM Piloto p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Piloto.findByEdad", query = "SELECT p FROM Piloto p WHERE p.edad = :edad")})
public class Piloto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "numero")
    private String numero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "edad")
    private short edad;
    @JoinColumn(name = "fk_equipo", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Equipo fkEquipo;
    @JoinColumn(name = "fk_motocicleta", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Motocicleta fkMotocicleta;
    @JoinColumn(name = "fk_nacionalidad", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Nacionalidad fkNacionalidad;

    public Piloto() {
    }

    public Piloto(String numero) {
        this.numero = numero;
    }

    public Piloto(String numero, String nombre, short edad) {
        this.numero = numero;
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    public Equipo getFkEquipo() {
        return fkEquipo;
    }

    public void setFkEquipo(Equipo fkEquipo) {
        this.fkEquipo = fkEquipo;
    }

    public Motocicleta getFkMotocicleta() {
        return fkMotocicleta;
    }

    public void setFkMotocicleta(Motocicleta fkMotocicleta) {
        this.fkMotocicleta = fkMotocicleta;
    }

    public Nacionalidad getFkNacionalidad() {
        return fkNacionalidad;
    }

    public void setFkNacionalidad(Nacionalidad fkNacionalidad) {
        this.fkNacionalidad = fkNacionalidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numero != null ? numero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Piloto)) {
            return false;
        }
        Piloto other = (Piloto) object;
        if ((this.numero == null && other.numero != null) || (this.numero != null && !this.numero.equals(other.numero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
    
}
