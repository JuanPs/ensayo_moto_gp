/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.motogp.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author juand
 */
@Entity
@Table(name = "motocicleta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Motocicleta.findAll", query = "SELECT m FROM Motocicleta m")
    , @NamedQuery(name = "Motocicleta.findByCodigo", query = "SELECT m FROM Motocicleta m WHERE m.codigo = :codigo")
    , @NamedQuery(name = "Motocicleta.findByColor", query = "SELECT m FROM Motocicleta m WHERE m.color = :color")})
public class Motocicleta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "codigo")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "color")
    private String color;
    @JoinColumn(name = "fk_marca", referencedColumnName = "codigo")
    @ManyToOne(optional = false)
    private Marca fkMarca;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkMotocicleta")
    private List<Piloto> pilotoList;

    public Motocicleta() {
    }

    public Motocicleta(String codigo) {
        this.codigo = codigo;
    }

    public Motocicleta(String codigo, String color) {
        this.codigo = codigo;
        this.color = color;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Marca getFkMarca() {
        return fkMarca;
    }

    public void setFkMarca(Marca fkMarca) {
        this.fkMarca = fkMarca;
    }

    @XmlTransient
    public List<Piloto> getPilotoList() {
        return pilotoList;
    }

    public void setPilotoList(List<Piloto> pilotoList) {
        this.pilotoList = pilotoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Motocicleta)) {
            return false;
        }
        Motocicleta other = (Motocicleta) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + codigo + " ";
    }
    
}
