/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jugadores.controlador;

import com.listajugadores.pojo.Jugador;
import com.listajugadores.pojo.NodoJugador;
import com.listajugadores.pojo.NodoPirinola;
import com.listajugadores.pojo.Pirinola;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author user
 */
@Named(value = "listaPirinolaController")
@SessionScoped
public class ListaPirinolaController implements Serializable {
ListaDECircularPirinola listaPirinola= new ListaDECircularPirinola();
ListaSECirculaJugadores listaJugador= new ListaSECirculaJugadores();
ListaJugadoresController listaControlJuga= new ListaJugadoresController();
private NodoJugador cabeza;
private Pirinola pirinolaAdicionar;
private Jugador pos1;
private Jugador pos2;
private int b;
private int c;

    private String listado;
    private int bolsa=0;
    private int a;
    private int kase;
    NodoPirinola temp= new NodoPirinola(pirinolaAdicionar);

    public ListaDECircularPirinola getListaPirinola() {
        return listaPirinola;
    }

    public void setListaPirinola(ListaDECircularPirinola listaPirinola) {
        this.listaPirinola = listaPirinola;
    }

    public ListaSECirculaJugadores getListaJugador() {
        return listaJugador;
    }

    public void setListaJugador(ListaSECirculaJugadores listaJugador) {
        this.listaJugador = listaJugador;
    }

    public ListaJugadoresController getListaControlJuga() {
        return listaControlJuga;
    }

    public void setListaControlJuga(ListaJugadoresController listaControlJuga) {
        this.listaControlJuga = listaControlJuga;
    }

    public NodoJugador getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoJugador cabeza) {
        this.cabeza = cabeza;
    }

    public Pirinola getPirinolaAdicionar() {
        return pirinolaAdicionar;
    }

    public void setPirinolaAdicionar(Pirinola pirinolaAdicionar) {
        this.pirinolaAdicionar = pirinolaAdicionar;
    }



    public Jugador getPos1() {
        return pos1;
    }

    public void setPos1(Jugador pos1) {
        this.pos1 = pos1;
    }

    public Jugador getPos2() {
        return pos2;
    }

    public void setPos2(Jugador pos2) {
        this.pos2 = pos2;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }

    public int getBolsa() {
        return bolsa;
    }

    public void setBolsa(int bolsa) {
        this.bolsa = bolsa;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getKase() {
        return kase;
    }

    public void setKase(int kase) {
        this.kase = kase;
    }

    public NodoPirinola getTemp() {
        return temp;
    }

    public void setTemp(NodoPirinola temp) {
        this.temp = temp;
    }
    
    /**
     * Creates a new instance of ListaPirinolaController
     */
    public ListaPirinolaController() {
        adicionarPirinola("Pon 1", true, false, (short)1);
        adicionarPirinola("Toma1", false, true, (short)1);
        adicionarPirinola("Pon 2", true, false, (short) 2);
        adicionarPirinola("Toma 2", false, true, (short)2);
        adicionarPirinola("Toma todo", false, false, (short) bolsa);
        adicionarPirinola("Todos ponen", true, true,(short)1);
        
        mostrarListado();
        temp=listaPirinola.getCabeza();
    }
    public void mostrarListado(){
        listado=listaPirinola.listarNodos();
    }
    public void adicionarPirinola(String descripcion, boolean estado,boolean estado2, short valor){
        Pirinola pirinolita= new Pirinola(descripcion, estado, estado2, valor);
        listaPirinola.adicionarNodoP(pirinolita);
        
    }
    public void adicionarAlFinal(){
        if(pirinolaAdicionar.getValor()<=0){
            
        }
        else{
            listaPirinola.adicionarNodoP(pirinolaAdicionar);
            irPrimero();
            
        }
    }
    public Pirinola recorrerListaCircularDerecha(){
        pos1=listaControlJuga.getTemp().getDato();
        int cont=1;
        int pos=0;
        int cuanto=listaPirinola.cuantoGira();
        while(temp!=null){
            if(cont==cuanto){
                temp.getDato();
                pos1.setNumeroFichas((short) a);
                
                operaciones(temp.getDato().isEstado(),temp.getDato().isEstado2());
                pos1.setNumeroFichas((short) getC());
                
                listaControlJuga.irSiguiente();
                pos1=listaControlJuga.getTemp().getDato();
                return temp.getDato();
            }
            temp=temp.getSiguiente();
            cont++;
        }
        return null;
    }
    
    public Pirinola recorrerListaPirinolaIzquierda(){
        pos1=listaControlJuga.getTemp().getDato();
        int cont=0;
        int pos=0;
        int cuantos=listaPirinola.cuantoGira();
        
        while(temp!=null){
            if(cont==cuantos){
                temp.getDato();
                pos1.setNumeroFichas((short) a);
                
                operaciones(temp.getDato().isEstado(),temp.getDato().isEstado2());
                pos1.setNumeroFichas((short) getC());
                listaControlJuga.irAnterior();
                pos1=listaControlJuga.getTemp().getDato();
                return temp.getDato();
            }
            temp=temp.getAnterior();
            cont++;
        }
        return null;
    }
    public void irSiguiente(){
        if(temp.getSiguiente()!=null){
            temp=temp.getSiguiente();
        }
    }
    public void irAnterior(){
        if(temp.getAnterior()!=null){
            temp=temp.getAnterior();
        }
        
    }
    public void irPrimero(){
        temp=listaPirinola.getCabeza();
    }
    public void sumORestarfichas(int kase){
        int b;
        int c;
        NodoJugador temp1= listaControlJuga.getTemp();
        while(temp1.getSiguiente()!= listaControlJuga.getLista().getCabeza()){
            if(temp1.getDato().getNumeroFichas()>0){
                a=temp1.getDato().getNumeroFichas()-getKase();
                temp1.getDato().setNumeroFichas((short) a);
                temp1=temp1.getSiguiente();
            }
            if(temp1.getSiguiente()==listaControlJuga.getLista().getCabeza()){
                a=temp1.getDato().getNumeroFichas()-getKase();
                temp1.getDato().setNumeroFichas((short) a);
        }
            b=getKase()* listaControlJuga.getLista().contarNodos();
            bolsa=b;
    }

} 
    public void operaciones(boolean estado,boolean estado2){
        //toma
        if(temp.getDato().isEstado()== false & temp.getDato().isEstado2()==true){
            if(bolsa<0){
                bolsa=bolsa-temp.getDato().getValor();
                c=pos1.getNumeroFichas()+temp.getDato().getValor();
            }
        }
        //poner
        if(temp.getDato().isEstado()==true & temp.getDato().isEstado2()==false){
        if(pos1.getNumeroFichas()>=temp.getDato().getValor()){
            bolsa=bolsa+temp.getDato().getValor();
            c=pos1.getNumeroFichas()-temp.getDato().getValor();            
        }
        else{
            c=pos1.getNumeroFichas();
            bolsa=bolsa+c;
                    
        }
    }
        //toma todo
        if(temp.getDato().isEstado()==false&temp.getDato().isEstado2()==false){
            if(bolsa>0){
                b=pos1.getNumeroFichas()+bolsa;
                bolsa=0;
            }
            
        }
    }
}

