/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jugadores.controlador;

import com.listajugadores.pojo.Jugador;
import com.listajugadores.pojo.NodoJugador;
import com.listajugadores.pojo.NodoPirinola;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author user
 */
@Named(value = "listaJugadoresController")
@SessionScoped
public class ListaJugadoresController implements Serializable {

    private ListaSECirculaJugadores lista= new ListaSECirculaJugadores();
    private ListaDECircularPirinola listap= new ListaDECircularPirinola();
    private NodoJugador temp;
    private NodoPirinola temp1;
    private String listado;
    private boolean verNuevo=false;
    private short bolsa=0;
    private int kase;
    private boolean deshabilitarJugar= false;
    private boolean habilitarMenuJuego= true;
    private Jugador jugadorcito;
    private boolean deshabilitarEleccion=false;

    public NodoJugador getTemp() {
        return temp;
    }

    public void setTemp(NodoJugador temp) {
        this.temp = temp;
    }

    public String getListado() {
        return listado;
    }

    public void setListado(String listado) {
        this.listado = listado;
    }
    

    public ListaSECirculaJugadores getLista() {
        return lista;
    }

    public void setLista(ListaSECirculaJugadores lista) {
        this.lista = lista;
    }

    public ListaDECircularPirinola getListap() {
        return listap;
    }

    public void setListap(ListaDECircularPirinola listap) {
        this.listap = listap;
    }

    public NodoPirinola getTemp1() {
        return temp1;
    }

    public void setTemp1(NodoPirinola temp1) {
        this.temp1 = temp1;
    }

    public short getBolsa() {
        return bolsa;
    }

    public void setBolsa(short bolsa) {
        this.bolsa = bolsa;
    }

    public int getKase() {
        return kase;
    }

    public void setKase(int kase) {
        this.kase = kase;
    }

    public boolean isVerNuevo() {
        return verNuevo;
    }

    public void setVerNuevo(boolean verNuevo) {
        this.verNuevo = verNuevo;
    }

    public boolean isDeshabilitarJugar() {
        return deshabilitarJugar;
    }

    public void setDeshabilitarJugar(boolean deshabilitarJugar) {
        this.deshabilitarJugar = deshabilitarJugar;
    }

    public boolean isHabilitarMenuJuego() {
        return habilitarMenuJuego;
    }

    public void setHabilitarMenuJuego(boolean habilitarMenuJuego) {
        this.habilitarMenuJuego = habilitarMenuJuego;
    }

    public Jugador getJugadorcito() {
        return jugadorcito;
    }

    public void setJugadorcito(Jugador jugadorcito) {
        this.jugadorcito = jugadorcito;
    }

    public boolean isDeshabilitarEleccion() {
        return deshabilitarEleccion;
    }

    public void setDeshabilitarEleccion(boolean deshabilitarEleccion) {
        this.deshabilitarEleccion = deshabilitarEleccion;
    }
    
    
    
    
    
    
    
    /**
     * Creates a new instance of ListaJugadoresController
     */
    public ListaJugadoresController() {
        
        adicionarJugador("Juan David Arias Arias",(short)20);
        adicionarJugador("Victoria Gutierrez",(short)20);
        adicionarJugador("Juan Carlos Ramirez",(short)20);
        adicionarJugador("Andrés Felipe Buitrago",(short)20);
        
        temp=lista.getCabeza();
    }
    public void mostrarListado(){
        listado=lista.listarNodos();
    }
    
    public void adicionarJugador (String nombre, short numeroFichas){
        Jugador jug= new Jugador(nombre, numeroFichas);
        
        lista.adicionarNodo(jug);
        
    }
    public void adicionarAlInicio(String nombre, short numeroFichas){
        Jugador jug=new Jugador(nombre, numeroFichas);
        
        lista.adicionarNodoInicial(jug);
    }

    public void irSiguiente(){
        if(lista.getCabeza()!=null){
            temp=temp.getSiguiente();
    }
    }
    public void irAnterior(){
        if(temp.getAnterior()!=null){
            temp=temp.getAnterior();
        }
    }
    public void irPrimero(){
        if(lista.getCabeza()!=null){
            temp=lista.getCabeza();
        }
    }
    
    public void irUltimo(){
        if(lista.getCabeza()!=null){
            temp=lista.obtenerUltimo();
        }            
    }
    
    public void sumResfichas(int kase){
        NodoJugador temp=lista.getCabeza();
        deshabilitarJugar= true;
        while(temp.getSiguiente()!=lista.getCabeza()){
            if(temp.getDato().getNumeroFichas()>getKase()){
                temp.getDato().setNumeroFichas((short)(temp.getDato().getNumeroFichas()-getKase()));
                bolsa=(short) (bolsa+getKase());
            }
            else{
                bolsa=(short) (bolsa+temp.getDato().getNumeroFichas());
                temp.getDato().setNumeroFichas((short)0);
            }
            temp=temp.getSiguiente();
        }
        if(temp.getSiguiente()==lista.getCabeza()){
                temp.getDato().setNumeroFichas((short)(temp.getDato().getNumeroFichas()-getKase()));
                bolsa=(short) (bolsa+getKase());
                habilitarMenuJuego=false;
        }
    }
    public Jugador iniciador(){
        deshabilitarEleccion=true;
        int cont=0;
        int cantidad=lista.aleatorio();
        while (temp!=null){
            if(cont==cantidad){
                jugadorcito=temp.getDato();
                return temp.getDato();
            }
            temp=temp.getSiguiente();
            cont++;
        }
        return null;
    }

}
    
 

    

   

