/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jugadores.controlador;

import com.listajugadores.pojo.Jugador;
import com.listajugadores.pojo.NodoJugador;
import java.io.Serializable;

/**
 *
 * @author user
 */


public class ListaSECirculaJugadores implements Serializable{
    private NodoJugador cabeza;

    public ListaSECirculaJugadores() {
    }

    public NodoJugador getCabeza() {
        return cabeza;
    }
    
    public String adicionarNodo(Jugador info){
        if (this.cabeza==null){
            cabeza= new NodoJugador(info);
            cabeza.setSiguiente(cabeza);
        }
        else{
            NodoJugador temp=cabeza;
            while(temp.getSiguiente()!=cabeza ){
                temp=temp.getSiguiente();
            }
            temp.setSiguiente(new NodoJugador(info) );
            temp.getSiguiente().setSiguiente(cabeza);
        }
        return "Jugador adicionado exitosamente";
}
    
    public String adicionarNodoInicial(Jugador info){
        if(this.cabeza==null){
            cabeza= new NodoJugador(info);
            cabeza.setSiguiente(cabeza);
        }
        else{
            NodoJugador temp= cabeza;
            while(temp.getSiguiente()!=cabeza){
                temp=temp.getSiguiente();
               
            }
            temp.setSiguiente(new NodoJugador(info));
            temp.getSiguiente().setSiguiente(cabeza);
            cabeza=temp.getSiguiente();
        }
    return "adicionado al inicio exitosamente";
    }
    
    public String listarNodos(){
        String listado ="";
        if(cabeza==null){
            return "la lista está vacía";   
        }
        else{
            NodoJugador temp=cabeza;
            while(temp.getSiguiente()!=cabeza){
                listado+=temp.getDato();
                temp=temp.getSiguiente();
                
            }
            return listado;
        }
    }
    public NodoJugador obtenerUltimo(){
        if(cabeza!=null){
            NodoJugador temp= cabeza;
            while(temp.getSiguiente()!=cabeza){
                temp=temp.getSiguiente();
            }
            return temp;
        }
        return null;
    }
    
    public int aleatorio(){
        int answer=(int)(Math.random()*100+1);
        return answer;
    }
    

    public int contarNodos(){
        if(cabeza==null){
            return 0;
        }
        else{
            NodoJugador temp=cabeza;
            int cont=1;
            while(temp.getSiguiente()!=cabeza){
                cont++;
                temp=temp.getSiguiente();
            }
            return cont;
        }
    }
    public int obtenerPosiciones(int posicion){
        NodoJugador temp=cabeza;
        int cont=1;
        while(cont<posicion){
            temp=temp.getSiguiente();
            cont++;
        }
        return posicion;
    }

}
