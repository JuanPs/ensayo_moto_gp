/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jugadores.controlador;

import com.listajugadores.pojo.NodoPirinola;
import com.listajugadores.pojo.Pirinola;

/**
 *
 * @author user
 */
public class ListaDECircularPirinola {
    
    private NodoPirinola cabeza;

    public ListaDECircularPirinola() {
    }

    public NodoPirinola getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoPirinola cabeza) {
        this.cabeza = cabeza;
    }
    


    
    public String adicionarNodoP(Pirinola info){
        if(this.cabeza==null){
            cabeza=new NodoPirinola(info);
            cabeza.setSiguiente(cabeza);
            cabeza.setAnterior(cabeza);
            
        }
        else{
            NodoPirinola temp=cabeza;
            while(temp.getSiguiente()!=cabeza){
                temp=temp.getSiguiente();
            }
            temp.setSiguiente(new NodoPirinola(info));
            temp.getSiguiente().setAnterior(temp);
            temp.getSiguiente().setSiguiente(cabeza);
            cabeza.setAnterior(temp.getSiguiente());
        }
        return "Adicionado exitosamente";
    }
    public String listarNodos(){
        String listado="";
        if(cabeza==null){
            return "la lista se encuentra vacía";
        }
        else{
        NodoPirinola temp=cabeza;
        do{
            listado+=temp.getDato();
            temp=temp.getSiguiente();
        }
        while(temp!=cabeza);
        return listado;
    }
    }
    public int cuantoGira(){
        int answer = (int) (Math.random()*100+1);
        return answer;
    }
}
